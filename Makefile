dump-dados-001:
	docker exec -it moviefy-db pg_dump --column-inserts --no-owner --username=postgres -h127.0.0.1 --port 5432 postgres > ./database/01_DADOS.sql

dump: dump-dados-001
	$(info Done)
