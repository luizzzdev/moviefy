package com.moviefy.repositories;

import com.moviefy.entities.Comment;
import com.moviefy.entities.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByMovieOrderByCreatedAtDesc(Movie movie);
}
