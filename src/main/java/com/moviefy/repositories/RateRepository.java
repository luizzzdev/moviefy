package com.moviefy.repositories;

import com.moviefy.entities.Movie;
import com.moviefy.entities.Rate;
import com.moviefy.entities.User;
import com.moviefy.entities.pk.RatePK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateRepository extends JpaRepository<Rate, RatePK> {
    Rate findByIdUserAndIdMovie(User user, Movie movie);
}
