package com.moviefy.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moviefy.entities.Category;
import com.moviefy.entities.Comment;
import com.moviefy.entities.Movie;
import com.moviefy.entities.Rate;

import java.io.Serializable;
import java.time.Instant;
import java.util.*;

public class MovieCommentsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT")
    private Instant releaseDate;
    private String photoUrl;

    private Set<Category> categories = new HashSet<>();
    private List<CommentDTO> comments = new ArrayList<>();
    private Set<Rate> rates = new HashSet<>();

    public MovieCommentsDTO() {
    }

    public MovieCommentsDTO(Long id, String name, String description, Instant releaseDate, String photoUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.releaseDate = releaseDate;
        this.photoUrl = photoUrl;
    }

    public MovieCommentsDTO(Movie movie) {
        this.id = movie.getId();
        this.name = movie.getName();
        this.description = movie.getDescription();
        this.releaseDate = movie.getReleaseDate();
        this.photoUrl = movie.getPhotoUrl();

        for (Comment comment : movie.getComments()) this.comments.add(new CommentDTO(comment));
        this.categories.addAll(movie.getCategories());
        this.rates.addAll(movie.getRates());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Instant releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public Double getRates() {
        if (rates.size() == 0) return 0.0;
        Integer sum = 0;
        for (Rate rate : rates) sum += rate.getRate();
        return (sum * 1.0) / rates.size();
    }

    public Movie toEntity() {
        Movie movie = new Movie(id, name, description, releaseDate, photoUrl);
        for (Category category : categories) movie.getCategories().add(category);
        for (CommentDTO comment : comments) movie.getComments().add(comment.toEntity());
        return movie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieCommentsDTO movieDTO = (MovieCommentsDTO) o;
        return Objects.equals(id, movieDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
