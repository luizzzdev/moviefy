package com.moviefy.dtos;

import java.io.Serializable;
import java.time.Instant;

import com.moviefy.entities.Comment;
import com.moviefy.entities.User;
import com.moviefy.entities.UserCommentLike;

public class UserCommentLikeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    private User user;
    private Comment comment;
    private Instant instant;
    
    public UserCommentLikeDTO() {
    	
    }

	public UserCommentLikeDTO(User user, Comment comment, Instant instant) {
		super();
		this.user = user;
		this.comment = comment;
		this.instant = instant;
	}
	
	public UserCommentLikeDTO(UserCommentLike entity) {
		this.user = entity.getId().getUser();
		this.comment = entity.getId().getComment();
		this.instant = entity.getInstant();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
	public Instant getInstant() {
		return instant;
	}

	public void setInstant(Instant instant) {
		this.instant = instant;
	}

	public UserCommentLike toEntity() {
    	return new UserCommentLike(user, comment, instant);
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCommentLikeDTO other = (UserCommentLikeDTO) obj;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
    
  
    
}
