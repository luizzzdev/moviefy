package com.moviefy.dtos;

import java.io.Serializable;

public class RateInsertDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer rate;
	
	public RateInsertDTO() {
		
	}

	public RateInsertDTO(Integer rate) {
		super();
		this.rate = rate;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rate == null) ? 0 : rate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RateInsertDTO other = (RateInsertDTO) obj;
		if (rate == null) {
			if (other.rate != null)
				return false;
		} else if (!rate.equals(other.rate))
			return false;
		return true;
	}
	

}
