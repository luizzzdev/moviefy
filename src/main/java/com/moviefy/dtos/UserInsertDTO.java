package com.moviefy.dtos;

import java.io.Serializable;

import com.moviefy.entities.User;

public class UserInsertDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long id;
    private String name;
    private String email;
    private String password;
    
    public UserInsertDTO () {
    	
    }

	public UserInsertDTO(Long id, String name, String email, String password) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
	}
    
    public UserInsertDTO(User entity) {
    	this.id = entity.getId();
    	this.name =  entity.getName();
    	this.email = entity.getEmail();
    	this.password = entity.getPassword();
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
    public User toEntity () {
    	return new User(id, name, email, password);
    }
	
}
