package com.moviefy.dtos;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.moviefy.entities.Movie;
import com.moviefy.entities.Playlist;

public class PlaylistDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private Set<MovieDTO> movies = new HashSet<>();

    public PlaylistDTO() {
    }

    public PlaylistDTO(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public PlaylistDTO(Playlist playlist) {
        this.id = playlist.getId();
        this.title = playlist.getTitle();
        for (Movie movie : playlist.getMovies()) movies.add(new MovieDTO(movie));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<MovieDTO> getMovies() {
        return movies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlaylistDTO that = (PlaylistDTO) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
