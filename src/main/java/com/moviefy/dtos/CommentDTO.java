package com.moviefy.dtos;

import java.io.Serializable;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moviefy.entities.Comment;
import com.moviefy.entities.User;

public class CommentDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String text;
    private String userName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm", timezone = "GMT")
    private Instant createdAt;

    public CommentDTO() {
    }

    public CommentDTO(Long id, String text, String userName, Instant createdAt) {
        this.id = id;
        this.text = text;
        this.userName = userName;
        this.createdAt = createdAt;
    }

    public CommentDTO(Comment comment) {
        this.id = comment.getId();
        this.text = comment.getText();
        this.createdAt = comment.getCreatedAt();
        this.userName = comment.getUser().getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Comment toEntity() {
        User user = new User(null, userName, null, null);
        return new Comment(id, text, createdAt, null, user);
    }
}
