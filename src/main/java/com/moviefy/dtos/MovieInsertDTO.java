package com.moviefy.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moviefy.entities.Category;
import com.moviefy.entities.Movie;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class MovieInsertDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String description;
    private Instant releaseDate;
    private String photoUrl;
    private Set<Category> categories = new HashSet<>();

    public MovieInsertDTO() {
    }

    public MovieInsertDTO(Long id, String name, String description, Instant releaseDate, String photoUrl, Set<Category> categories) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.releaseDate = releaseDate;
        this.photoUrl = photoUrl;
        this.categories = categories;
    }

    public MovieInsertDTO(Movie movie) {
        this.id = movie.getId();
        this.name = movie.getName();
        this.description = movie.getDescription();
        this.releaseDate = movie.getReleaseDate();
        this.photoUrl = movie.getPhotoUrl();
        this.categories = movie.getCategories();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Instant getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Instant releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieInsertDTO that = (MovieInsertDTO) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public Movie toEntity() {
        Movie movie = new Movie(id, name, description, releaseDate, photoUrl);
        for (Category category : categories) movie.getCategories().add(category);
        return movie;
    }
}