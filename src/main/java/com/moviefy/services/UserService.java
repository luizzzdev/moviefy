package com.moviefy.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moviefy.dtos.UserDTO;
import com.moviefy.dtos.UserInsertDTO;
import com.moviefy.entities.User;
import com.moviefy.repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository repository;
	
	public List<UserDTO> findAll() {
		List<User> list = repository.findAll();
		return list.stream().map(e -> new UserDTO(e)).collect(Collectors.toList());
	}
	
	public UserDTO findById(Long id) throws Exception {
		Optional<User> user = repository.findById(id);
		User entity = user.orElseThrow(() -> new Exception("User not found"));
		return new UserDTO(entity);
	
	}
	
	public UserDTO insert(UserInsertDTO dto) {
		User user = dto.toEntity();
		user.setPassword(passwordEncoder.encode(dto.getPassword()));
		return new UserDTO(repository.save(user));
	}
	
	@Transactional
	public UserDTO update(Long id, UserDTO userDto) throws Exception {
		try {
			User entity = repository.getOne(id);
			updateData(entity, userDto);
			entity = repository.save(entity);
			return new UserDTO(entity);
		}catch (Exception e){
			throw new Exception(e);
		}
	}
	
	public void updateData(User entity, UserDTO userDto) {
		entity.setName(userDto.getName());
		entity.setEmail(userDto.getEmail());
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = repository.findByEmail(email);

		if(user == null) {
			throw new UsernameNotFoundException(email);
		}

		return user;
	}
}
