package com.moviefy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moviefy.dtos.MovieDTO;
import com.moviefy.dtos.PlaylistDTO;
import com.moviefy.entities.Movie;
import com.moviefy.entities.Playlist;
import com.moviefy.repositories.PlaylistRepository;

@Service
public class PlaylistService {
    @Autowired
    private PlaylistRepository repository;

	public Page<PlaylistDTO> findAllPaged(PageRequest pageRequest) {
		Page<Playlist> playlists = repository.findAll(pageRequest);
        return playlists.map(PlaylistDTO::new);
	}
	
	@Transactional
	public PlaylistDTO findById(Long id) {
		return new PlaylistDTO(repository.getOne(id));
	}
    
    public PlaylistDTO insert(PlaylistDTO dto) {
        Playlist playlist = new Playlist(null, dto.getTitle());
        for (MovieDTO movie : dto.getMovies()) playlist.getMovies().add(new Movie(movie.getId(), null, null, null, null));
        repository.save(playlist);
        return new PlaylistDTO(playlist);
    }
    
}
