package com.moviefy.services;

import com.moviefy.dtos.MovieDTO;
import com.moviefy.entities.Movie;
import com.moviefy.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moviefy.dtos.CommentDTO;
import com.moviefy.dtos.UserCommentLikeDTO;
import com.moviefy.entities.Comment;
import com.moviefy.repositories.CommentRepository;
import com.moviefy.repositories.UserCommentLikeRepository;

import java.time.Instant;
import java.util.List;

@Service
public class CommentService {
    @Autowired
    private CommentRepository repository;

    @Autowired
    private UserCommentLikeRepository userCommentLikeRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private MovieService movieService;

    public CommentDTO insert(CommentDTO commentDTO, Long movieId) {
        User user = authService.authenticated();
        MovieDTO movie = movieService.findMovieById(movieId);
        Comment comment = new Comment();
        comment.setText(commentDTO.getText());
        comment.setMovie(movie.toEntity());
        comment.setUser(user);
        comment.setCreatedAt(Instant.now());
        return new CommentDTO(repository.save(comment));
    }

    @Transactional
    public CommentDTO findCommentById(Long id) {
        return new CommentDTO(repository.getOne(id));
    }

    public UserCommentLikeDTO insertLike(Long commentId) {
        User user = authService.authenticated();
        CommentDTO comment = this.findCommentById(commentId);
        UserCommentLikeDTO userCommentLike = new UserCommentLikeDTO(user, comment.toEntity(), Instant.now());
        return new UserCommentLikeDTO(userCommentLikeRepository.save(userCommentLike.toEntity()));
    }

    public List<Comment> findByMovieOrderDescByDate(Movie movie) {
        return repository.findByMovieOrderByCreatedAtDesc(movie);
    }
}
