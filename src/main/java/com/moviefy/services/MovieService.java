package com.moviefy.services;

import com.moviefy.dtos.MovieCommentsDTO;
import com.moviefy.dtos.MovieDTO;
import com.moviefy.dtos.MovieInsertDTO;
import com.moviefy.dtos.RateInsertDTO;
import com.moviefy.entities.Comment;
import com.moviefy.entities.Movie;
import com.moviefy.entities.Rate;
import com.moviefy.entities.User;
import com.moviefy.repositories.MovieRepository;
import com.moviefy.repositories.RateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class MovieService {
    @Autowired
    private MovieRepository repository;

    @Autowired
    private RateRepository rateRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private CommentService commentService;

    public Page<MovieDTO> findAllPaged(PageRequest pageRequest, String name) {
        Page<Movie> movies;

        if (name.equals("")) {
            movies = repository.findAll(pageRequest);
        } else {
            movies = repository.findByNameContainingIgnoreCase(name, pageRequest);
        }
        return movies.map(MovieDTO::new);
    }

    @Transactional
    public MovieDTO findMovieById(Long id) {
        return new MovieDTO(repository.getOne(id));
    }

    @Transactional
    public MovieCommentsDTO findById(Long id) {
        Movie movie = repository.getOne(id);
        List<Comment> comments = commentService.findByMovieOrderDescByDate(movie);
        movie.getComments().clear();
        movie.getComments().addAll(comments);
        return new MovieCommentsDTO(movie);
    }

    public MovieInsertDTO insert(MovieInsertDTO dto) {
        Movie newMovie = repository.save(dto.toEntity());
        return new MovieInsertDTO(newMovie);
    }

    public Rate insertMovieAvaliation(RateInsertDTO rateInsertDTO, Long movieId) {
        User user = authService.authenticated();
        MovieDTO movie = this.findMovieById(movieId);
        Rate rate = new Rate(user, movie.toEntity(), rateInsertDTO.getRate(), Instant.now(), Instant.now());
        return rateRepository.save(rate);
    }

    @Transactional
    public Rate userMovieRate(Long movieId) {
        User user = authService.authenticated();
        Movie movie = repository.getOne(movieId);
        return rateRepository.findByIdUserAndIdMovie(user, movie);
    }
}
