package com.moviefy.services.exceptions;

public class JWTAuthorizationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public JWTAuthorizationException(String message) {
        super(message);
    }
} 