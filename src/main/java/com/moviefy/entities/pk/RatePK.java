package com.moviefy.entities.pk;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.moviefy.entities.Movie;
import com.moviefy.entities.User;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RatePK implements Serializable {
    private static final Long serialVersionUID = 1L;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RatePK ratePK = (RatePK) o;
        return Objects.equals(movie, ratePK.movie) &&
                Objects.equals(user, ratePK.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(movie, user);
    }
}
