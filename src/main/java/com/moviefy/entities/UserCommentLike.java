package com.moviefy.entities;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.moviefy.entities.pk.UserCommentLikePK;

@Entity
@Table(name = "tb_user_comment_like")
public class UserCommentLike implements Serializable {
    private static final Long serialVersionUID = 1L;

    @EmbeddedId
    private UserCommentLikePK id = new UserCommentLikePK();

    private Instant instant;

    public UserCommentLike() {}

    public UserCommentLike(User user, Comment comment, Instant instant) {
        this.id.setUser(user);
        this.id.setComment(comment);
        this.instant = instant;
    }

    public UserCommentLikePK getId() {
        return id;
    }

    public void setId(UserCommentLikePK id) {
        this.id = id;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCommentLike that = (UserCommentLike) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
