package com.moviefy.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.moviefy.services.AuthService;
import com.moviefy.services.CommentService;

@RestController
@RequestMapping(value = "/comments")
public class CommentResource {

    @Autowired
    private AuthService authService;

    @Autowired
    private CommentService commentService;

    @PostMapping("/{id}/like")
    public ResponseEntity<Void> like(@PathVariable Long id) {
        commentService.insertLike(id);
        return ResponseEntity.noContent().build();
    }

}
