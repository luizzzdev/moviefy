package com.moviefy.resources;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.moviefy.dtos.PlaylistDTO;
import com.moviefy.services.PlaylistService;

@RestController
@RequestMapping(value = "/playlists")
public class PlaylistResource {
    @Autowired
    private PlaylistService service;

    @GetMapping
    public ResponseEntity<Page<PlaylistDTO>> findAllPaged(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                          @RequestParam(value = "size", defaultValue = "10") Integer size,
                                                          @RequestParam(value = "direction", defaultValue = "ASC") String direction,
                                                          @RequestParam(value = "orderBy", defaultValue = "title") String orderBy) {

        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.valueOf(direction), orderBy);
        return ResponseEntity.ok().body(service.findAllPaged(pageRequest));
    }
    
   @GetMapping(value = "/{id}")
   public ResponseEntity<PlaylistDTO> findById(@PathVariable Long id) {
	  return ResponseEntity.ok().body(service.findById(id));
   }

    @PostMapping
    public ResponseEntity<PlaylistDTO> insert(@RequestBody PlaylistDTO dto) {
        PlaylistDTO playlistDTO = service.insert(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(playlistDTO.getId()).toUri();

        return ResponseEntity.created(uri).body(playlistDTO);
    }

}
