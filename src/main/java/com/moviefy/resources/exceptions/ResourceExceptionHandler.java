package com.moviefy.resources.exceptions;

import com.moviefy.services.exceptions.JWTAuthenticationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ResourceExceptionHandler {
    @ExceptionHandler(JWTAuthenticationException.class)
    public ResponseEntity<StandardError> jwtAuthorization(JWTAuthenticationException exception, HttpServletRequest request) {
        String error = "Authentication error.";
        HttpStatus httpStatus = HttpStatus.FORBIDDEN;
        Integer status = httpStatus.value();

        StandardError err = new StandardError(Instant.now(), status, error, exception.getMessage(), request.getRequestURI());

        return ResponseEntity.status(httpStatus).body(err);
    }
}
