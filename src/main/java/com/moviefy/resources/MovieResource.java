package com.moviefy.resources;

import com.moviefy.dtos.*;
import com.moviefy.entities.Rate;
import com.moviefy.services.CommentService;
import com.moviefy.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(value = "/movies")
public class MovieResource {

    @Autowired
    private MovieService service;

    @Autowired
    private CommentService commentService;

    @GetMapping
    public ResponseEntity<Page<MovieDTO>> findAllPaged(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                       @RequestParam(value = "size", defaultValue = "10") Integer size,
                                                       @RequestParam(value = "direction", defaultValue = "ASC") String direction,
                                                       @RequestParam(value = "orderBy", defaultValue = "name") String orderBy,
                                                       @RequestParam(value = "name", defaultValue = "") String name
    ) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.valueOf(direction), orderBy);
        return ResponseEntity.ok().body(service.findAllPaged(pageRequest, name));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MovieCommentsDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(service.findById(id));
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<MovieInsertDTO> insert(@RequestBody MovieInsertDTO dto) {
        MovieInsertDTO movieInsertDTO = service.insert(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(movieInsertDTO.getId()).toUri();

        return ResponseEntity.created(uri).body(movieInsertDTO);
    }

    @PostMapping("/{movieId}/comment")
    public ResponseEntity<CommentDTO> insertComment(@PathVariable Long movieId, @RequestBody CommentDTO dto) {
        CommentDTO newComment = commentService.insert(dto, movieId);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(newComment.getId()).toUri();

        return ResponseEntity.created(uri).body(newComment);
    }

    @PostMapping("/{movieId}/avaliation")
    public ResponseEntity<Void> insertMovieAvaliation(@PathVariable Long movieId, @RequestBody RateInsertDTO userRate) {
        service.insertMovieAvaliation(userRate, movieId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{movieId}/my-rate")
    public ResponseEntity<Rate> userMovieRate(@PathVariable Long movieId) {
        Rate rate = service.userMovieRate(movieId);
        if (rate == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok().body(rate);
    }
}
