package com.moviefy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviefyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviefyApplication.class, args);
	}

}
