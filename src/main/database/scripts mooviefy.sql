
/* 1)*/
/* a)*/
DELIMITER |
create procedure sp_insert_playlist(playlistName varchar(255))
begin
insert into tb_playlist values
(null, playlistName);
END |
DELIMITER ;

/* b)*/
DELIMITER |
create procedure sp_insert_movie(movieName varchar(255), releaseDate date)
begin
insert into tb_movie values
(null, movieName, "",  releaseDate);
END |
DELIMITER ;

/* c) */
delimiter |	
create	procedure playlistByName(var_nome_playlist varchar(30))	
begin	
set	@query=CONCAT('select * from tb_playlist where title like "%', var_nome_playlist,'%"');	
PREPARE	listar_dados FROM @query;
EXECUTE	listar_dados;	
end	|
delimiter ;	

DROP PROCEDURE playlistByName;

call playlistByName('hor');	

/* 2)*/
DELIMITER |		
create function func_raterPerUser(userId int)
returns int begin
declare @rates int;
set @rates = ( select count(user_id) from tb_rate where user_id = userId )
return @rates;
END |
DELIMITER ;


DELIMITER |
create function func_mostReviewedMovie()
returns varchar(255) begin
declare @movieName varchar(255)
set @movieName = ( select m.name, count(*) as times FROM tb_movie m
inner join tb_rate r
on  r.movie_id = m.id
group by m.name
order by times desc ;
)
return @movieName;
END |
DELIMITER ;

DELIMITER |
create function func_mostActiveUser()
returns int begin
declare @userId int
set @userId = ( select u.name, count(*) as times from tb_user u
inner join tb_rate r
on  u.id = r.user_id
group by r.user_id
order by times desc;
)
return @movieName;
END |
DELIMITER ;

# 3
DELIMITER $
CREATE TRIGGER nome_filme before insert on tb_movie for each row
BEGIN
    if(length(new.name) < 3)
		THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Nome do filme deve ter pelo menos 3 digitos';
	END IF;
END $
DELIMITER ;


DELIMITER $
CREATE TRIGGER texto_comentario before insert on tb_comment for each row
BEGIN
    if(length(new.text) < 3)
		THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'O texto do comentario deve ter pelo menos 3 digitos';
	END IF;
END $
DELIMITER ;


CREATE TABLE tb_rate_audit (
created_at timestamp,
rate int(11),
updated_at timestamp,
movie_id bigint NOT NULL,
user_id bigint NOT NULL
);


DELIMITER $
CREATE TRIGGER avaliacao_filme before update on tb_rate for each row
BEGIN
    INSERT INTO tb_rate_audit(created_at, rate, updated_at, movie_id, user_id)
     VALUES(old.created_at, old.rate, new.updated_at, old.movie_id, old.user_id);
END $
DELIMITER ;


