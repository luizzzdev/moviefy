package com.moviefy;

import com.moviefy.dtos.MovieCommentsDTO;
import com.moviefy.dtos.MovieDTO;
import com.moviefy.resources.MovieResource;
import com.moviefy.services.MovieService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.awt.print.Pageable;
import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovieResourceTest {
    @Autowired
    private WebApplicationContext context;

    @MockBean
    private MovieService movieService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void listAllMovies() throws Exception {
        PageRequest pageRequest = PageRequest.of(0, 10);

        given(movieService.findAllPaged(pageRequest, "")).willReturn(Page.empty());
        mockMvc.perform(get("/movies"))
                .andExpect(status().isOk());
    }

    @Test
    public void findMovieById() throws Exception {
        given(movieService.findById(1L)).willReturn(new MovieCommentsDTO(1L, null, null, null, null));
        mockMvc.perform(get("/movies/1"))
                .andExpect(status().isOk());
    }
}
