package com.moviefy;

import com.moviefy.dtos.CredentialsDTO;
import com.moviefy.dtos.TokenDTO;
import com.moviefy.services.AuthService;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthResourceTest {
    @Autowired
    private WebApplicationContext context;

    @MockBean
    private AuthService authService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void login() throws Exception {
        CredentialsDTO credentialsDTO = new CredentialsDTO("luiz@email.com.br", "luiz123");
        given(authService.authenticate(credentialsDTO)).willReturn(new TokenDTO("luiz@email.com.br", "123456"));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", credentialsDTO.getEmail());
        jsonObject.put("password", credentialsDTO.getPassword());
        mockMvc.perform(post("/auth/login").contentType(MediaType.APPLICATION_JSON).content(jsonObject.toString()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
