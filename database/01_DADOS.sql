--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Debian 11.5-3.pgdg90+1)
-- Dumped by pg_dump version 11.5 (Debian 11.5-3.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tb_category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_category (
    id bigint NOT NULL,
    name character varying(255)
);


--
-- Name: tb_category_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tb_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tb_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tb_category_id_seq OWNED BY public.tb_category.id;


--
-- Name: tb_comment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_comment (
    id bigint NOT NULL,
    created_at timestamp without time zone,
    text character varying(255),
    movie_id bigint,
    user_id bigint
);


--
-- Name: tb_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tb_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tb_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tb_comment_id_seq OWNED BY public.tb_comment.id;


--
-- Name: tb_movie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_movie (
    id bigint NOT NULL,
    description character varying(255),
    name character varying(255),
    photo_url character varying(255),
    release_date date
);


--
-- Name: tb_movie_category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_movie_category (
    movie_id bigint NOT NULL,
    category_id bigint NOT NULL
);


--
-- Name: tb_movie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tb_movie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tb_movie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tb_movie_id_seq OWNED BY public.tb_movie.id;


--
-- Name: tb_playlist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_playlist (
    id bigint NOT NULL,
    title character varying(255)
);


--
-- Name: tb_playlist_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tb_playlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tb_playlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tb_playlist_id_seq OWNED BY public.tb_playlist.id;


--
-- Name: tb_playlist_movie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_playlist_movie (
    playlist_id bigint NOT NULL,
    movie_id bigint NOT NULL
);


--
-- Name: tb_rate; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_rate (
    created_at timestamp without time zone,
    rate integer,
    updated_at timestamp without time zone,
    movie_id bigint NOT NULL,
    user_id bigint NOT NULL
);


--
-- Name: tb_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_role (
    id bigint NOT NULL,
    authority character varying(255)
);


--
-- Name: tb_role_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tb_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tb_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tb_role_id_seq OWNED BY public.tb_role.id;


--
-- Name: tb_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_user (
    id bigint NOT NULL,
    email character varying(255),
    name character varying(255),
    password character varying(255)
);


--
-- Name: tb_user_comment_like; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_user_comment_like (
    instant timestamp without time zone,
    user_id bigint NOT NULL,
    comment_id bigint NOT NULL
);


--
-- Name: tb_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tb_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tb_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tb_user_id_seq OWNED BY public.tb_user.id;


--
-- Name: tb_user_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tb_user_role (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
);


--
-- Name: tb_category id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_category ALTER COLUMN id SET DEFAULT nextval('public.tb_category_id_seq'::regclass);


--
-- Name: tb_comment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_comment ALTER COLUMN id SET DEFAULT nextval('public.tb_comment_id_seq'::regclass);


--
-- Name: tb_movie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_movie ALTER COLUMN id SET DEFAULT nextval('public.tb_movie_id_seq'::regclass);


--
-- Name: tb_playlist id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_playlist ALTER COLUMN id SET DEFAULT nextval('public.tb_playlist_id_seq'::regclass);


--
-- Name: tb_role id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_role ALTER COLUMN id SET DEFAULT nextval('public.tb_role_id_seq'::regclass);


--
-- Name: tb_user id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user ALTER COLUMN id SET DEFAULT nextval('public.tb_user_id_seq'::regclass);


--
-- Data for Name: tb_category; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_category (id, name) VALUES (1, 'Science Fiction');
INSERT INTO public.tb_category (id, name) VALUES (2, 'Horror');
INSERT INTO public.tb_category (id, name) VALUES (3, 'Crime');
INSERT INTO public.tb_category (id, name) VALUES (4, 'Drama');
INSERT INTO public.tb_category (id, name) VALUES (5, 'Biography');
INSERT INTO public.tb_category (id, name) VALUES (6, 'History');
INSERT INTO public.tb_category (id, name) VALUES (7, 'Comedy');
INSERT INTO public.tb_category (id, name) VALUES (8, 'Romance');
INSERT INTO public.tb_category (id, name) VALUES (9, 'Action');
INSERT INTO public.tb_category (id, name) VALUES (10, 'Thriller');
INSERT INTO public.tb_category (id, name) VALUES (11, 'Adventure');
INSERT INTO public.tb_category (id, name) VALUES (12, 'War');
INSERT INTO public.tb_category (id, name) VALUES (13, 'Fantasy');
INSERT INTO public.tb_category (id, name) VALUES (14, 'Animation');


--
-- Data for Name: tb_comment; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_comment (id, created_at, text, movie_id, user_id) VALUES (1, '1971-12-31 21:00:00', 'Such a good movie!', 1, 1);
INSERT INTO public.tb_comment (id, created_at, text, movie_id, user_id) VALUES (2, '1971-12-31 21:00:00', 'Such a bad movie!', 2, 2);


--
-- Data for Name: tb_movie; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (1, 'Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a Wookiee and two droids to save the galaxy from the Empire''s world-destroying battle station, while also attempting to rescue Princess Leia from the mysterious Darth Vader.', 'Star Wars', 'https://images-na.ssl-images-amazon.com/images/I/51c6S4kGFmL.jpg', '1971-12-31');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (2, 'After a space merchant vessel perceives an unknown transmission as a distress call, its landing on the source moon finds one of the crew attacked by a mysterious lifeform, and they soon realize that its life cycle has merely begun.', 'Alien', 'https://upload.wikimedia.org/wikipedia/en/f/fb/Aliens_poster.jpg', '1976-12-31');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (3, 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.', 'The Godfather', 'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR3,0,182,268_AL_.jpg', '1972-09-10');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (4, 'In German-occupied Poland during World War II, industrialist Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.', 'Schindler''s List', 'https://m.media-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', '1994-03-11');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (5, 'A jury holdout attempts to prevent a miscarriage of justice by forcing his colleagues to reconsider the evidence.', '12 Angry Men', 'https://m.media-amazon.com/images/M/MV5BMWU4N2FjNzYtNTVkNC00NzQ0LTg0MjAtYTJlMjFhNGUxZDFmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_UX182_CR0,0,182,268_AL_.jpg', '1958-10-10');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (6, 'When an open-minded Jewish librarian and his son become victims of the Holocaust, he uses a perfect mixture of will, humor, and imagination to protect his son from the dangers around their camp.', 'La vita è bella', 'https://m.media-amazon.com/images/M/MV5BYmJmM2Q4NmMtYThmNC00ZjRlLWEyZmItZTIwOTBlZDQ3NTQ1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg', '1999-02-05');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (7, 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.', 'The Shawshank Redemption', 'https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg', '1995-03-17');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (8, 'A struggling salesman takes custody of his son as he s poised to begin a life-changing professional career.', 'The Pursuit of Happyness', 'https://m.media-amazon.com/images/M/MV5BMTQ5NjQ0NDI3NF5BMl5BanBnXkFtZTcwNDI0MjEzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg', '2007-02-02');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (9, 'After he becomes a quadriplegic from a paragliding accident, an aristocrat hires a young man from the projects to be his caregiver.', 'Intouchables', 'https://m.media-amazon.com/images/M/MV5BMTYxNDA3MDQwNl5BMl5BanBnXkFtZTcwNTU4Mzc1Nw@@._V1_UX182_CR0,0,182,268_AL_.jpg', '2012-08-31');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (10, 'The drug-induced utopias of four Coney Island people are shattered when their addictions run deep.', 'Requiem for a Dream', 'https://m.media-amazon.com/images/M/MV5BOTdiNzJlOWUtNWMwNS00NmFlLWI0YTEtZmI3YjIzZWUyY2Y3XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', '2001-11-02');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (11, 'After John Nash, a brilliant but asocial mathematician, accepts secret work in cryptography, his life takes a turn for the nightmarish.', 'Beatiful Mind', 'https://m.media-amazon.com/images/M/MV5BMzcwYWFkYzktZjAzNC00OGY1LWI4YTgtNzc5MzVjMDVmNjY0XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg', '2002-02-15');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (12, 'A retired CIA agent travels across Europe and relies on his old skills to save his estranged daughter, who has been kidnapped while on a trip to Paris.', 'Taken', 'https://m.media-amazon.com/images/M/MV5BMTM4NzQ0OTYyOF5BMl5BanBnXkFtZTcwMDkyNjQyMg@@._V1_UX182_CR0,0,182,268_AL_.jpg', '2008-10-03');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (13, 'A family heads to an isolated hotel for the winter where a sinister presence influences the father into violence, while his psychic son sees horrific forebodings from both past and future.', 'The Shining', 'https://m.media-amazon.com/images/M/MV5BZWFlYmY2MGEtZjVkYS00YzU4LTg0YjQtYzY1ZGE3NTA5NGQxXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg', '1980-12-25');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (14, 'A former Roman General sets out to exact vengeance against the corrupt emperor who murdered his family and sent him into slavery.', 'Gladiator', 'https://m.media-amazon.com/images/M/MV5BMDliMmNhNDEtODUyOS00MjNlLTgxODEtN2U3NzIxMGVkZTA1L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', '2000-05-19');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (15, 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.', 'The Dark Knight', 'https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg', '2008-07-18');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (16, 'A Polish Jewish musician struggles to survive the destruction of the Warsaw ghetto of World War II.', 'The Pianist', 'https://m.media-amazon.com/images/M/MV5BOWRiZDIxZjktMTA1NC00MDQ2LWEzMjUtMTliZmY3NjQ3ODJiXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY268_CR6,0,182,268_AL_.jpg', '2003-03-07');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (17, 'A seventeen-year-old aristocrat falls in love with a kind but poor artist aboard the luxurious, ill-fated R.M.S. Titanic.', 'Titanic', 'https://m.media-amazon.com/images/M/MV5BMDdmZGU3NDQtY2E5My00ZTliLWIzOTUtMTY4ZGI1YjdiNjk3XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_UX182_CR0,0,182,268_AL_.jpg', '1998-01-16');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (18, 'When his secret bride is executed for assaulting an English soldier who tried to rape her, William Wallace begins a revolt against King Edward I of England.', 'Braveheart', 'https://m.media-amazon.com/images/M/MV5BMzkzMmU0YTYtOWM3My00YzBmLWI0YzctOGYyNTkwMWE5MTJkXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg', '1995-07-14');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (19, 'A boy who communicates with spirits seeks the help of a disheartened child psychologist.', 'The Sixth Sense', 'https://m.media-amazon.com/images/M/MV5BMWM4NTFhYjctNzUyNi00NGMwLTk3NTYtMDIyNTZmMzRlYmQyXkEyXkFqcGdeQXVyMTAwMzUyOTc@._V1_UX182_CR0,0,182,268_AL_.jpg', '1999-10-22');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (20, 'Following the Normandy Landings, a group of U.S. soldiers go behind enemy lines to retrieve a paratrooper whose brothers have been killed in action.', 'Saving Private Ryan', 'https://m.media-amazon.com/images/M/MV5BZjhkMDM4MWItZTVjOC00ZDRhLThmYTAtM2I5NzBmNmNlMzI1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UY268_CR0,0,182,268_AL_.jpg', '1998-09-11');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (21, 'The lives of guards on Death Row are affected by one of their charges: a black man accused of child murder and rape, yet who has a mysterious gift.', 'The Green Mile', 'https://m.media-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1_UX182_CR0,0,182,268_AL_.jpg', '2000-03-10');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (22, 'A pragmatic Paleontologist visiting an almost complete theme park is tasked with protecting a couple of kids after a power failure causes the park s cloned dinosaurs to run loose.', 'Jurassic Park', 'https://m.media-amazon.com/images/M/MV5BMjM2MDgxMDg0Nl5BMl5BanBnXkFtZTgwNTM2OTM5NDE@._V1_UX182_CR0,0,182,268_AL_.jpg', '1993-06-25');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (23, 'A cyborg, identical to the one who failed to kill Sarah Connor, must now protect her teenage son, John Connor, from a more advanced and powerful cyborg.', 'Terminator 2: Judgment Day', 'https://m.media-amazon.com/images/M/MV5BMGU2NzRmZjUtOGUxYS00ZjdjLWEwZWItY2NlM2JhNjkxNTFmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', '1991-08-30');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (24, 'After his son is captured in the Great Barrier Reef and taken to Sydney, a timid clownfish sets out on a journey to bring him home.', 'Finding Nemo', 'https://m.media-amazon.com/images/M/MV5BZTAzNWZlNmUtZDEzYi00ZjA5LWIwYjEtZGM1NWE1MjE4YWRhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', '2003-04-03');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (25, 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.', 'The Matrix', 'https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', '1999-05-21');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (26, 'A man with short-term memory loss attempts to track down his wife s murderer.', 'Memento', 'https://m.media-amazon.com/images/M/MV5BZTcyNjk1MjgtOWI3Mi00YzQwLWI5MTktMzY4ZmI2NDAyNzYzXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY268_CR0,0,182,268_AL_.jpg', '2001-12-31');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (27, 'A former neo-nazi skinhead tries to prevent his younger brother from going down the same wrong path that he did', 'American History X', 'https://m.media-amazon.com/images/M/MV5BZjA0MTM4MTQtNzY5MC00NzY3LWI1ZTgtYzcxMjkyMzU4MDZiXkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UX182_CR0,0,182,268_AL_.jpg', '1999-04-16');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (28, 'Two detectives, a rookie and a veteran, hunt a serial killer who uses the seven deadly sins as his motives.', 'Se7en', 'https://m.media-amazon.com/images/M/MV5BOTUwODM5MTctZjczMi00OTk4LTg3NWUtNmVhMTAzNTNjYjcyXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg', '1995-12-15');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (29, 'The story of Henry Hill and his life in the mob, covering his relationship with his wife Karen Hill and his mob partners Jimmy Conway and Tommy DeVito in the Italian-American crime syndicate.', 'Goodfellas', 'https://m.media-amazon.com/images/M/MV5BY2NkZjEzMDgtN2RjYy00YzM1LWI4ZmQtMjIwYjFjNmI3ZGEwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg', '1990-12-21');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (30, 'When a killer shark unleashes chaos on a beach community, its up to a local sheriff, a marine biologist, and an old seafarer to hunt the beast down.', 'Jaws', 'https://m.media-amazon.com/images/M/MV5BMmVmODY1MzEtYTMwZC00MzNhLWFkNDMtZjAwM2EwODUxZTA5XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UX182_CR0,0,182,268_AL_.jpg', '1975-12-25');
INSERT INTO public.tb_movie (id, description, name, photo_url, release_date) VALUES (31, 'In 1984 East Berlin, an agent of the secret police, conducting surveillance on a writer and his lover, finds himself becoming increasingly absorbed by their lives.', 'Das Leben der Anderen', 'https://m.media-amazon.com/images/M/MV5BOThkM2EzYmMtNDE3NS00NjlhLTg4YzktYTdhNzgyOWY3ZDYzXkEyXkFqcGdeQXVyNzQzNzQxNzI@._V1_UY268_CR3,0,182,268_AL_.jpg', '2007-11-30');


--
-- Data for Name: tb_movie_category; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (1, 1);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (2, 1);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (2, 2);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (3, 3);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (3, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (4, 5);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (4, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (4, 6);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (5, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (6, 7);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (6, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (6, 8);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (7, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (8, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (8, 5);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (9, 5);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (9, 7);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (9, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (10, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (11, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (11, 5);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (12, 9);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (12, 10);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (13, 2);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (13, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (14, 9);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (14, 11);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (14, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (15, 9);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (15, 3);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (15, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (16, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (16, 5);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (17, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (17, 8);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (18, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (18, 5);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (18, 6);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (19, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (19, 10);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (20, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (20, 12);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (21, 3);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (21, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (21, 13);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (22, 1);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (22, 11);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (22, 9);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (23, 1);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (23, 9);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (24, 14);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (24, 11);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (24, 7);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (25, 1);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (25, 9);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (26, 10);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (27, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (28, 3);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (28, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (29, 3);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (29, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (29, 5);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (30, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (30, 10);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (30, 11);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (31, 4);
INSERT INTO public.tb_movie_category (movie_id, category_id) VALUES (31, 10);


--
-- Data for Name: tb_playlist; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_playlist (id, title) VALUES (1, 'Space Movies');
INSERT INTO public.tb_playlist (id, title) VALUES (2, 'Horror Movies');


--
-- Data for Name: tb_playlist_movie; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_playlist_movie (playlist_id, movie_id) VALUES (1, 1);
INSERT INTO public.tb_playlist_movie (playlist_id, movie_id) VALUES (1, 2);
INSERT INTO public.tb_playlist_movie (playlist_id, movie_id) VALUES (2, 2);


--
-- Data for Name: tb_rate; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_rate (created_at, rate, updated_at, movie_id, user_id) VALUES ('1971-12-31 21:00:00', 5, NULL, 1, 1);
INSERT INTO public.tb_rate (created_at, rate, updated_at, movie_id, user_id) VALUES ('1971-12-31 21:00:00', 3, NULL, 2, 2);


--
-- Data for Name: tb_role; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_role (id, authority) VALUES (1, 'ROLE_ADMIN');
INSERT INTO public.tb_role (id, authority) VALUES (2, 'ROLE_CLIENT');


--
-- Data for Name: tb_user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_user (id, email, name, password) VALUES (1, 'luiz@email.com.br', 'luiz', '$2a$10$YbEO2lY43941VVdBlNZPourVQt4bebWFG0ogtFSSHgRqITQ0t3kQ6');
INSERT INTO public.tb_user (id, email, name, password) VALUES (2, 'lukas@email.com.br', 'lukas', '$2a$10$u2Q7pOMgsR.iEe.fxET7ou2hOEMCFwvQLz2.uTWpPvtZMMI9v4ECi');
INSERT INTO public.tb_user (id, email, name, password) VALUES (3, 'cleber@email.com.br', 'Cleber', '$2a$10$YJYgYSKXBc.S2tEeCtEtUe0xMt4fkcqpx/xLv1cLvlKu4OIzp9vIq');


--
-- Data for Name: tb_user_comment_like; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_user_comment_like (instant, user_id, comment_id) VALUES ('1971-12-31 21:00:00', 2, 1);


--
-- Data for Name: tb_user_role; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tb_user_role (user_id, role_id) VALUES (1, 1);
INSERT INTO public.tb_user_role (user_id, role_id) VALUES (1, 2);
INSERT INTO public.tb_user_role (user_id, role_id) VALUES (2, 1);
INSERT INTO public.tb_user_role (user_id, role_id) VALUES (2, 2);
INSERT INTO public.tb_user_role (user_id, role_id) VALUES (3, 2);


--
-- Name: tb_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tb_category_id_seq', 14, true);


--
-- Name: tb_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tb_comment_id_seq', 2, true);


--
-- Name: tb_movie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tb_movie_id_seq', 31, true);


--
-- Name: tb_playlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tb_playlist_id_seq', 2, true);


--
-- Name: tb_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tb_role_id_seq', 2, true);


--
-- Name: tb_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tb_user_id_seq', 3, true);


--
-- Name: tb_category tb_category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_category
    ADD CONSTRAINT tb_category_pkey PRIMARY KEY (id);


--
-- Name: tb_comment tb_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_comment
    ADD CONSTRAINT tb_comment_pkey PRIMARY KEY (id);


--
-- Name: tb_movie_category tb_movie_category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_movie_category
    ADD CONSTRAINT tb_movie_category_pkey PRIMARY KEY (movie_id, category_id);


--
-- Name: tb_movie tb_movie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_movie
    ADD CONSTRAINT tb_movie_pkey PRIMARY KEY (id);


--
-- Name: tb_playlist_movie tb_playlist_movie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_playlist_movie
    ADD CONSTRAINT tb_playlist_movie_pkey PRIMARY KEY (playlist_id, movie_id);


--
-- Name: tb_playlist tb_playlist_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_playlist
    ADD CONSTRAINT tb_playlist_pkey PRIMARY KEY (id);


--
-- Name: tb_rate tb_rate_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_rate
    ADD CONSTRAINT tb_rate_pkey PRIMARY KEY (movie_id, user_id);


--
-- Name: tb_role tb_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_role
    ADD CONSTRAINT tb_role_pkey PRIMARY KEY (id);


--
-- Name: tb_user_comment_like tb_user_comment_like_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user_comment_like
    ADD CONSTRAINT tb_user_comment_like_pkey PRIMARY KEY (comment_id, user_id);


--
-- Name: tb_user tb_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user
    ADD CONSTRAINT tb_user_pkey PRIMARY KEY (id);


--
-- Name: tb_user_role tb_user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user_role
    ADD CONSTRAINT tb_user_role_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: tb_user uk_4vih17mube9j7cqyjlfbcrk4m; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user
    ADD CONSTRAINT uk_4vih17mube9j7cqyjlfbcrk4m UNIQUE (email);


--
-- Name: tb_comment fk45c1cuqlljd60ihc9j0962ekq; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_comment
    ADD CONSTRAINT fk45c1cuqlljd60ihc9j0962ekq FOREIGN KEY (user_id) REFERENCES public.tb_user(id);


--
-- Name: tb_user_role fk7vn3h53d0tqdimm8cp45gc0kl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user_role
    ADD CONSTRAINT fk7vn3h53d0tqdimm8cp45gc0kl FOREIGN KEY (user_id) REFERENCES public.tb_user(id);


--
-- Name: tb_user_comment_like fk9ab119ef0l6jjog1miwvxglul; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user_comment_like
    ADD CONSTRAINT fk9ab119ef0l6jjog1miwvxglul FOREIGN KEY (user_id) REFERENCES public.tb_user(id);


--
-- Name: tb_playlist_movie fk9o34ft7cgey7wd4becw59cqj1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_playlist_movie
    ADD CONSTRAINT fk9o34ft7cgey7wd4becw59cqj1 FOREIGN KEY (movie_id) REFERENCES public.tb_movie(id);


--
-- Name: tb_movie_category fkb7890fxbttgpkfhj6l70lufct; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_movie_category
    ADD CONSTRAINT fkb7890fxbttgpkfhj6l70lufct FOREIGN KEY (category_id) REFERENCES public.tb_category(id);


--
-- Name: tb_user_comment_like fkdfx5vm1wsyiq37v5wpuv8bbam; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user_comment_like
    ADD CONSTRAINT fkdfx5vm1wsyiq37v5wpuv8bbam FOREIGN KEY (comment_id) REFERENCES public.tb_comment(id);


--
-- Name: tb_user_role fkea2ootw6b6bb0xt3ptl28bymv; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_user_role
    ADD CONSTRAINT fkea2ootw6b6bb0xt3ptl28bymv FOREIGN KEY (role_id) REFERENCES public.tb_role(id);


--
-- Name: tb_rate fkfbf1dm3vuj91omd9yfnfa9j6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_rate
    ADD CONSTRAINT fkfbf1dm3vuj91omd9yfnfa9j6 FOREIGN KEY (user_id) REFERENCES public.tb_user(id);


--
-- Name: tb_comment fki79tsew4u3uuilsvu6363c5ve; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_comment
    ADD CONSTRAINT fki79tsew4u3uuilsvu6363c5ve FOREIGN KEY (movie_id) REFERENCES public.tb_movie(id);


--
-- Name: tb_rate fklo8ojpxh96qe3xnaexvutfrkp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_rate
    ADD CONSTRAINT fklo8ojpxh96qe3xnaexvutfrkp FOREIGN KEY (movie_id) REFERENCES public.tb_movie(id);


--
-- Name: tb_movie_category fkprne9epfedk2wi5iibjjj94jd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_movie_category
    ADD CONSTRAINT fkprne9epfedk2wi5iibjjj94jd FOREIGN KEY (movie_id) REFERENCES public.tb_movie(id);


--
-- Name: tb_playlist_movie fkrb38alki8as4xxk4kmk9oq0jb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tb_playlist_movie
    ADD CONSTRAINT fkrb38alki8as4xxk4kmk9oq0jb FOREIGN KEY (playlist_id) REFERENCES public.tb_playlist(id);


--
-- PostgreSQL database dump complete
--

